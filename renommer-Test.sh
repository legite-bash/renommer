#!/bin/bash
# creer un repertoire /tmp/test/renommer
# avec des repertoires et fichiers test
declare -r renommer_bin="/www/bash/renommer/renommer.sh";
declare -r testDir="/tmp/test/renommer";


#########
# DEBUG #
#########
#evalCmd($cmd $lineno $txt)
evalCmd(){
    local ligneNu="";   if [ ! -z ${2+x} ]; then ligneNu="[$2]:"; fi
    local txt="";       if [ ! -z ${3+x} ]; then txt="$3"; fi

    echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
    eval "$1";
    return $?
}


echo "$LINENO:testDir=$testDir";
# on supprime le rep test existant
if [ -d "$testDir" ];
then
    rm -R "$testDir"
fi

# on (re)cré le rep test
evalCmd "mkdir -p $testDir"

if [ ! -d "$testDir" ];
then
    echo "Erreur lors de la creation de $testDir";
    exit 1
fi


createTest(){
    #showDebug $LINENO: createTest()";
    echo "Creation des repertoires et fichiers pour tester les filtres."
    echo "$LINENO:testDir=$testDir";

    # on supprime le rep test existant
    if [ -d "$testDir" ]; then rm -R "$testDir"; fi

    # on (re)cré le rep test
    evalCmd "mkdir -p $testDir"

    if [ ! -d "$testDir" ];
    then
        echo "Erreur lors de la creation de $testDir";
        exit 1
    fi

    evalCmd "cd $testDir"
}

createTest;

touch "a+b.ext"
touch "a b .ext"

touch "a [b].ext"


echo "########################"
evalCmd "$renommer_bin "

echo "########################"
evalCmd "$renommer_bin --showLibs " $lineno

echo "########################"
evalCmd "$renommer_bin --showLibs $testDir"

echo "########################"
evalCmd "$renommer_bin $testDir "

echo "########################"
evalCmd "$renommer_bin -d $testDir "

exit
echo "########################"
evalCmd "$renommer_bin $testDir "

echo "########################"
evalCmd "$renommer_bin $testDir "

echo "########################"
evalCmd "$renommer_bin $testDir "

