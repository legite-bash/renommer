#!/bin/bash
set -u;
# renommerfichier.sh
# renomme les fichier du repertoire donné en argument
# de facon recursive
# en ne suivant pas les liens sympboliques


declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`; # repertoire absolu du script (pas de slash de fin)
declare -r SCRIPT_FILENAME=${0##*/};         # nom.ext
declare -r SCRIPT_NAME=${SCRIPT_FILENAME%.*}   # uniquement le nom
declare -r SCRIPT_EXT="sh"                   # uniquement l'extention
VERSION="v0.0.4-rename";
#$?: retour de la derniere commande shell


################
# colorisation #
################
RESET_COLOR=$(tput sgr0)
BOLD=`tput smso`
NOBOLD=`tput rmso`

BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

NORMAL=$WHITE
INFO=$BLUE
CMD=$YELLOW
WARN=$RED
TITRE1=$GREEN
TITRE2=$MAGENTA
DEBUG_COLOR=$MAGENTA


####################
# codes de sorties #
####################
declare -r E_ARG_NONE=65
declare -r E_ARG_BAD=66 #perso


#############
# aide bash #
#############
#est ce que $sequence_lettres est dans "$mot" ?
#if echo "$mot" | grep -q "$sequence_lettres"
# L'option "-q" de grep supprime l'affichage du résultat.
#then
#  echo "$sequence_lettres trouvée dans $mot"
#else
#  echo "$sequence_lettres non trouvée dans $mot"
#fi


#############
# variables #
#    init   #
#############
# --- Definir si root ou user --- #
[ "$(id -u)" == "0" ] && declare -r IS_ROOT=1 || declare -r IS_ROOT=0;

# --- variables des includes --- #
declare -r UPDATE_HTTP="http://updates.legite.org/legite-install";

# --- variables generiques --- #
argNb=$#;
isUpdate=0;                     # demande de update
verbose=0;
isDebug=0;
isDebugShowVars=0;              # montrer les variables

# --- variables des parametres supplementaires --- #
PSP_sup="";                     # parametre supplementaire($@) => $1 $2 $n "$@""

declare -A fonctionsTbl;        # tableau contenant les fonctions (venant de PSP_sup)
isShowLibs=0;                    # affiche une liste des fonctions defini

# --- variables du projet --- #
isTest=0;
declare -r testDir="/tmp/test/renommer";

declare -A filtres; # tableau contenant les fonctions de filtre
filtreNo=0; #indique le numerio du filtre actif (pour isDebugage)
filtresActifs=""; #filtres qui ont été activés 
repertoire_source='';  # repertoire INITIAL contenant les fichier a renommer defini en argument ou a copier
repertoire_destination=''; # repertoire ou sera copié le repertoire source. uitilisé uniquement si -c présent
tabulation=" ";     # espace pour chaque sous rep
repertoireTotal=0;  # nombre total de repertoire scannés

profondeurNb=0;     # niveau de repertoire (le rep de base est compté)
declare -r PROFONDEUR_MAX=9;   # 0: desactive; 1: que le rep root 2: root+1 rep; etc
                    # ne pas depasser 9 car lit 10 comme 1
declare -A inodeCompteur; #tableau associatif
inodeCompteur['repertoires_lus']=0;
inodeCompteur['repertoires_modifies']=0;
inodeCompteur['fichiers_lus']=0;
inodeCompteur['fichiers_modifies']=0;
inodeCompteur['fichiers_supprimes']=0;
inodeCompteur['liens']=0;
inodeCompteur['repertoires_copies']=0;
inodeCompteur['repertoires_copies_existant']=0;
inodeCompteur['fichiers_copies']=0;
inodeCompteur['fichiers_copies_existant']=0;

isDeleted=0;        # fichier a supprimé (utiliser par les filtres)
isCopie=0;          # Le fonctiond de copie doit elle être appelé a la place de la fonction renommer?
isRecursif=0
isRename=0        # desactiver la fonction de renommage

# #################### #
# functions generiques #
# #################### #
usage (){
    if [ $argNb -eq 0 ];
    then
        echo "$SCRIPT_FILENAME [--showLibs] [--update] lib";
        echo " [-d] [--showVars] # fonctions de debug";
        echo " [--profondeur_max=n] -R repertoire_a_renommer";
        echo " [--profondeur_max=n] -R [--rename] -c repertoire_a_copier repertoire_cible";
        echo " --test filtre";
        echo " --profondeur_max 0:desactiver; 1: repertoire_source; n: root+n-1. defaut: $PROFONDEUR_MAX"
        exit $E_ARG_NONE;
    fi
}


showVars(){
if [ $isDebugShowVars -eq 1 ];
    then
        isDebugOld=$isDebug;
        isDebug=1;
        #showDebug " $LINENO"
        showDebug " argNb=$argNb"
        showDebug " IS_ROOT=$IS_ROOT"
        showDebug " isUpdate=$isUpdate"
        showDebug " verbose=$verbose"
        showDebug " isDebug=$isDebugOld"
        showDebug " isDebugShowVars=$isDebugShowVars"
        
        showDebug " isTest=$isTest"

        showDebug " repertoire_source='$repertoire_source'"
        showDebug " repertoire_destination='$repertoire_destination'"
        showDebug " verbose=$verbose"
        showDebug " isDebug=$isDebug"
        showDebug " isRecursif=$isRecursif"
        showDebug " isCopie=$isCopie"
        showDebug " isRename=$isRename"
        showDebug " tabulation='$tabulation'"
        showDebug " repertoireTotal=$repertoireTotal"
        showDebug " profondeurNb=$profondeurNb"
        showDebug " PROFONDEUR_MAX=$PROFONDEUR_MAX"
        echo "inodeCompteur:"
        showDebug " repertoires: ${inodeCompteur['repertoires_lus']}";
        showDebug " repertoires: ${inodeCompteur['repertoires_modifies']}";
        showDebug " fichiers   : ${inodeCompteur['fichiers_lus']}";
        showDebug " fichiers_modifies   : ${inodeCompteur['fichiers_modifies']}";
        showDebug " fichiers_supprimes  : ${inodeCompteur['fichiers_supprimes']}";
        showDebug " liens      : ${inodeCompteur['liens']}"
        showDebug " repertoires copies:          ${inodeCompteur['repertoires_copies']}";
        showDebug " repertoires copies existant: ${inodeCompteur['repertoires_copies_existant']}";
        showDebug " fichiers copies:             ${inodeCompteur['fichiers_copies']}";
        showDebug " fichier copies existant:     ${inodeCompteur['fichiers_copies_existant']}"
        showDebug " isShowLibs=$isShowLibs"
        showDebug " PSP_sup=$PSP_sup"

        isDebug=$isDebugOld;
    fi
}

#########
# DEBUG #
#########
#evalCmd($cmd $ligneNu $txt)
evalCmd(){
    local ligneNu="";   if [ ! -z ${2+x} ]; then ligneNu="[$2]:"; fi
    local txt="";       if [ ! -z ${3+x} ]; then txt="$3"; fi

    echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
    eval "$1";
    return $?
}


#showDebug(texte)
showDebug(){
if [ $isDebug -eq 1 ];
then
    #ligneNo=$1;
    texte="$1";
    if [ $# -eq 2 ]; then
        texte=$2;
    fi
    echo "${DEBUG_COLOR}$texte$NORMAL"
fi
}


################
# MISE A JOURS #
################
# telecharge la derniere version en supprimant la version local
#updateProg src dest
updateProg(){
    if [ $isUpdate -eq 1 ]
    then
        src=$1; dest=$2
        echo "${INFO}Update de $src$NORMAL"
        if [ $IS_ROOT -eq 1 ]
        then
            evalCmd "cp '$dest' '$dest.bak'" $LINENO;     # Fait une sauvegarde
            evalCmd "rm '$dest'";

            evalCmd "wget -O $dest $src"
            evalCmd "chmod +x $dest"
            if [ $? -ne 0 ];
            then
                echo "${WARN}Erreur lors de l'instalation de $dest.$NORMAL"
                evalCmd "mv '$dest.bak' '$dest' " 
            fi

        else
            echo "${WARN}Seul root peut updater (ecrire dans /usr/local/bin/, /root/.legite/)"
        fi
    fi
}

# - telecharge et installe la derniere version en remplacant le script qui a lancer l'update - #
selfUpdate (){
    if [ $isUpdate -eq 1 -a $IS_ROOT -eq 1  ];
    then
        if [ ! -d "$CONF_ETC_ROOT" ];then mkdir "$CONF_ETC_ROOT";fi
        if [ ! -d "$CONF_USER_ROOT" ];then mkdir "$CONF_USER_ROOT";fi
        echo "mise a jours du programme";
        #updateProg "$UPDATE_HTTP/$SCRIPT_FILENAME"   "/usr/local/bin/$SCRIPT_FILENAME"
        #updateProg "$UPDATE_HTTP/$CONF_ETC_FILENAME" "$CONF_ETC_PATH"
        #updateProg "$UPDATE_HTTP/$CONF_ORI_FILENAME" "$CONF_ORI_PATH"
        #updateProg "$UPDATE_HTTP/CONF_USER_FILENAME" "$CONF_USER_PATH"
        updateProg "$UPDATE_HTTP/renommer/renommer.sh" "/usr/local/bin/renommer.sh"
    fi
}


# telecharge la derniere version en supprimant la version local
#updateProg src dest
updateProg(){
    if [ $isUpdate -eq 1 ]
    then
        src=$1; dest=$2
        echo "$INFO update de $src$NORMAL"
        showDebug "$LINENO:destination=$dest"
        if [ $IS_ROOT -eq 1 ]
        then
            if [ -f "$dest" ];
            then
                evalCmd "mv $dest $dest.bak";
            fi

            evalCmd "wget -O $dest $src"
            evalCmd "chmod +x $dest"
        else
            echo "${WARN}Seul root peut updater (ecrire dans /usr/local/bin/, /root/.legite/)"
        fi
    fi
}


# - telecharge et installe la derniere version en remplacant le script qui a lancer l'update - #
selfUpdate (){
    if [ $isUpdate -eq 1 ]; then
        echo "mise a jours du programme";
        updateProg http://updates.legite.org/renommer/renommer.sh /usr/local/bin/renommer.sh
        exit
    fi
}
###############################
# GESTION DE LA CONFIGURATION #
###############################

# creer et execute les fichiers locaux
# LocalFileExec(localName){
execFile(){
    showDebug "execFile($@)";

    local localName="$1";
    if [  -f "$localName" ];
    then
        if [ ! -x "$localName" ];then evalCmd "chmod +x $localName;"; fi
        echo "nano $localName # Pour modifier le fichier"
        evalCmd ". $localName";
    else
        echo "${WARN}$localName n'existe pas.$NORMAL"
    fi
}


##########################
# GESTION DES LIBRAIRIES #
##########################
showLibs(){
    if [ $isShowLibs -eq 1 ];
    then
        local out="";
        for fichier_path in "${fonctionsTbl[@]}";
        do
            out="$out $fichier_path"
        done;
        echo "$out";
    fi
}

isLibExist(){
    local lib="$1"
    for fichier_path in "${fonctionsTbl[@]}";
    do
        if [ "$fichier_path" == "$lib" ]; then return 1; fi
    done;
    return 0;
}


# ################### #
# functions du projet #
# ################### #

#############
# les tests #
#############
lanceTest(){
    if [ $isTest -eq 1 ];
    then
        createTest
        renameRep $testDir;
        exit 0;
    fi
}


###############
# les filtres #
###############
##########################################
# filtre_supprimerFichierParasite(@)     #
# $@: tous les arg                       #
# utilise la variable globale $isDeleted #
##########################################
test_filtre_supprimerFichiersParasite(){
    touch "desktop.ini"
    touch "Thumbs.db"
    touch "AlbumArt-ex AlbumArt"
    touch "AlbumArtAlbumArt"
    touch "-AlbumArtAlbumArt"
}
filtres['supprimerFichiersParasite_test']=test_filtre_supprimerFichiersParasites;


filtre_supprimerFichiersParasite(){
    oriName="$@";
    isDeleted=0;
    case "$inode" in 
        "Desktop.ini")  isDeleted=1;  ;;
        "desktop.ini")  isDeleted=1;  ;;
        "Thumbs.db")    isDeleted=1;  ;;
    esac

    # suppression des AlbumArt*
    subAlbumArt=${inode:0:8};
    showDebug "${tabulation}$LINENO" "$subAlbumArt";
    if [ "$subAlbumArt" = "AlbumArt" ];
    then
        isDeleted=1;
    fi

    showDebug "${tabulation}$LINENO" "isDeleted=$isDeleted";
    if [ "$isDeleted" -eq 1 ];
    then
        echo "${tabulation}${WARN}Suppression de $inode.$NORMAL";
        rm "./$inode";
        ((inodeCompteur['fichiers_supprimes']++));
    fi
}
filtres['filtre_supprimerFichiersParasite']=filtre_supprimerFichiersParasite;


###########################
# filtre_renommerInode(@) #
# $@: tous les arg        #
###########################
filtre_renommerInode(){
    #http://www.tux-planet.fr/renommer-le-nom-des-fichiers-en-minuscule-sous-linux/
    oriName="$@";
    oldName="$oriName";
    showDebug "${tabulation}$LINENO filtre_renommerInode ('$oldName'->'$oriName)";

    filtreNo=0;
    filtresActifs="";
    # - SUPPRIMER LES CARACTERES SUIVANTS - #
    filtreNo="?";
    moveFile "$oldName" "$(echo -e "$oldName"  | tr -d '?')";
    filtreNo="!";
    moveFile "$oldName" "$(echo -e "$oldName"  | tr -d '!')";


    # - SUPPRIMER LES SEQUENCES SUIVANTES - #
    filtreNo=".\[emule-island.com\]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/.\[emule-island.com\]//g')";
    filtreNo="-emule-island.com-";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-emule-island.com-//g')";
    filtreNo=".\[emule-island.ru\]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/.\[emule-island.ru\]//g')";
    filtreNo="-emule-island.ru-";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-emule-island.ru-//g')";

    filtreNo="ZT -zone-telechargement.ws";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-zone-telechargement.ws//g')";
    filtreNo="ZT .Zone-Telechargement.Ws";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/.Zone-Telechargement.Ws//g')";
    filtreNo="ZT -WwW.Annuaire-Telechargement.CoM";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-WwW.Annuaire-Telechargement.CoM//g')";
    filtreNo="ZT -www.Zone-Telechargement.com";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-www.Zone-Telechargement.com//g')";
    filtreNo="ZT -.WwW.Zone-Telechargement.Net";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/.WwW.Zone-Telechargement.Net//g')";
    filtreNo="ZT -Annuaire-Telechargement.com";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-Annuaire-Telechargement.com//g')";
    filtreNo="ZT .WwW.Annuaire-Telechargement.COM";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/.WwW.Annuaire-Telechargement.COM//g')";
    filtreNo=".zone-telechargement.com";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/.zone-telechargement.com//g')";
    filtreNo=".WwW.Zone-Telechargement1.com";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/.WwW.Zone-Telechargement1.com//g')";
    filtreNo="[www.Cpasbien.me]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/\[www.Cpasbien.me\]_//g')";
    filtreNo="[www.Cpasbien.me]_";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/\[www.Cpasbien.me\]_//g')";

    filtreNo="-[found_via_ed2k-series.new\]";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-\[found_via_ed2k-series.new\]//g')";
    
    filtreNo="-www.CpasBien.io-";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-www.CpasBien.io-//g')";
    
    #filtreNo="";
    #moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's///g')";


    # changer les caracteres suivants #
    #filtreNo=;moveFile "$oldName" "$(echo -e "$oldName"  | tr '/' '-')"; #pose pb avec les rep
    filtreNo="\\";  moveFile "$oldName" "$(echo -e "$oldName"  | tr '\\' '-')";
    filtreNo=",";   moveFile "$oldName" "$(echo -e "$oldName"  | tr ',' '-')";
    filtreNo="’";   moveFile "$oldName" "$(echo -e "$oldName"  | tr '’' '-')";

    filtreNo="[";   moveFile "$oldName" "$(echo -e "$oldName"  | tr '[' '-')";
    filtreNo="]";   moveFile "$oldName" "$(echo -e "$oldName"  | tr ']' '-')";
    #filtreNo="]";moveFile "$oldName" "$(echo -e "$oldName"  | tr -d ']')";

    filtreNo="{";   moveFile "$oldName" "$(echo -e "$oldName"  | tr '{' '-')";
    #filtreNo="{";   moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/{/-/g')";
    #filtreNo="}";   moveFile "$oldName" "$(echo -e "$oldName"  | tr -d '}')";
    filtreNo="}";   moveFile "$oldName" "$(echo -e "$oldName"  | tr '}' '-')";

    filtreNo="(";   moveFile "$oldName" "$(echo -e "$oldName"  | tr '(' '-')";
    #filtreNo="{";   moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/{/-/g')";
    #filtreNo="}";   moveFile "$oldName" "$(echo -e "$oldName"  | tr -d '}')";
    filtreNo=")";   moveFile "$oldName" "$(echo -e "$oldName"  | tr ')' '-')";

    filtreNo="|";
    moveFile "$oldName" "$(echo -e "$oldName"  | tr '|' '-')";
    filtreNo=" : ";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/ : /-/g')";
    filtreNo=" , ";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/ , /-/g')";

    filtreNo=" - ";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/ - /-/g')";
    filtreNo=" -";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/ -/-/g')";
    filtreNo="- ";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/- /-/g')";
    filtreNo="--";
    moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/--/-/g')";

    filtreNo="ESPACE";
    moveFile "$oldName" "$(echo -e "$oldName"  | tr ' ' '_')";
    filtreNo="-";
    moveFile "$oldName" "$(echo -e "$oldName"  | tr "'" '_')";
    filtreNo="’";
    moveFile "$oldName" "$(echo -e "$oldName"  | tr "’" '_')";

    filtreNo='#';
    moveFile "$oldName" "$(echo -e "$oldName"  | tr '#' '-')";
    
    # filtre corrigeant le resultat des filtres precedents
    filtreNo="-_";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/-_/-/g')";
    filtreNo="_-";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/_-/-/g')";
    filtreNo="_-_";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/_-_/-/g')";
    filtreNo="._";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/\._/-/g')";
   #filtreNo=".-";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/\.-/-/g')"; #ATTENTION le '.' est un car special #NE PAS toucher au point (pb avec l'extention)
    filtreNo="__";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/__/_/g')";
    filtreNo="..";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/\.\./\./g')";

    filtreNo="_&_";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/_&_/_et_/g')";
    filtreNo="&";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/&/_et_/g')";

    filtreNo="[éèêë]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[éèêë]/e/g')";
    filtreNo="[ÉÈÊÊ]";#moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[ÉÈÊÊ]/E/g')";
    filtreNo="[îï]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[îï]/i/g')";
    filtreNo="[ÎÏ]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[ÎÏ]/I/g')";
    filtreNo="[ôö]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[ôö]/o/g')";
    filtreNo="[ÔÖ]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[ÔÖ]/O/g')";
    filtreNo="[ûüù]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[ûüù]/u/g')";
    filtreNo="[ÛÜ]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[ÛÜ]/U/g')";
    filtreNo="[âàä]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[âàä]/a/g')";
    filtreNo="[ÂÄÀ]";moveFile "$oldName" "$(echo -e "$oldName"  | sed -e 's/[ÂÄÀ]/A/g')";
    filtreNo="ç";moveFile "$oldName" "$(echo -e "$oldName"  | tr 'ç' 'c')";
    filtreNo="Ç";moveFile "$oldName" "$(echo -e "$oldName"  | tr 'Ç' 'C')";

    if [ "$oriName" == "$newName" ];
    then
        echo "${tabulation}\"$oriName\"";
    else
        echo "filtres: $filtresActifs";

        if [ -d "$newName" ];
        then
            echo "${tabulation}$CYAN\"$oriName\"";
            echo "${tabulation}\"$newName\"$NORMAL";
            ((inodeCompteur['repertoires_modifies']++));
        fi

        if [ -f "$newName" ];
        then
            echo "${tabulation}$CYAN\"$oriName\"";
            echo "${tabulation}\"$newName\"$NORMAL";
            ((inodeCompteur['fichiers_modifies']++));
        fi
    fi
}
filtres['filtre_renommerInode']=filtre_renommerInode;


setTabulation(){
    tabulation="";
    for (( t=1; t<=profondeurNb; t++ ))
    do
        tabulation="$tabulation  "
    done
}


# newName est une global, elle est modifié par la fonction
#moveFile(oldName, newName)
moveFile(){
    oldName="$1";
    newName="$2"
    #showDebug $LINENO "filtre $filtreNo:oldName=$oldName -> newName=$newName";

    #showDebug "${tabulation}$LINENO:filtreNo:$filtreNo";
    #echo "${tabulation}$LINENO:filtreNo:$filtreNo";

    if [ -f "$oldName" ];
    then
        if [[ "$oldName" != "$newName" ]];
        then
            #echo "${tabulation}Filtre:$filtreNo";
            filtresActifs="$filtresActifs '$filtreNo'"
            mv --no-clobber "$oldName" "$newName" 2>/dev/null
            oldName="$newName";
        fi
    #fi

    # tester si repertoire ajouter l'option --target-directory= de mv
    elif [ -d "$oldName" ];
    then
        if [[ "$oldName" != "$newName" ]];
        then
            echo "${tabulation}Filtre:$filtreNo";
            mv --no-clobber "$oldName" "$newName" 2>/dev/null
            oldName="$newName";
        fi
    fi
}


##############
# renameRep #
##############
renameRep(){
    repertoireLocal="$1";
    echo "${tabulation}${INFO}Repertoire([$profondeurNb/$PROFONDEUR_MAX|$repertoireTotal]$repertoireLocal)$NORMAL";
    if [ ! -d "$repertoireLocal" ]
    then
        #echo "$WARN $repertoire n'est pas un repertoire!$NORMAL"
        return
    fi

    ###################################################
    # - on se positionne dans le repertoire indiqué - #
    ###################################################
    cd "$repertoireLocal";
    cd_cr=$?; #code retour de cd
    #echo "${tabulation}$LINENO" "retour de cd: $cd_cr";
    
    if [ "$cd_cr" -ne 0 ];
    then
        # si le repertoire a pas été changer on quitte la fonction
        echo "${tabulation}${WARN}Problème lors de l'entrée dans ce repertoire (supprimer les espaces!)"
        return 1;
    fi

    ((profondeurNb++));
    setTabulation

    # - gestion du niveau de recursivité - #
    if [[ $profondeurNb > $PROFONDEUR_MAX ]]
    then
        echo "${tabulation}${WARN}Niveau max ($profondeurNb/$PROFONDEUR_MAX) de sous repertoire atteint $NORMAL$NOBOLD";
        return # on sort de la fonction
    fi

    ((repertoireTotal++));
    ((inodeCompteur['repertoires_lus']++));

    #showDebug "${tabulation}$LINENO rep courant($profondeurNb):$PWD";
    # - analyse du contenu du repertoire - #
    for inode in *
    do
        showDebug "${tabulation}$LINENO PWD=$PWD    inode=$inode";
        if [ -d "$inode" ];then
            if [ $isRecursif -eq 1 ];
            then
                repertoireActuel=$inode;
                #repertoireLocal="$repertoireLocal/$inode/";
                repertoireLocal="$PWD/$inode/";
                renameRep "$repertoireLocal"
            fi

        elif [ -f "$inode" ];then
            showDebug "${tabulation}$LINENO" "on traite l'inode.";
            ((inodeCompteur['fichiers_lus']++));

            # on supprime les fichiers parasites
            filtre_supprimerFichiersParasite

            if [ "$isDeleted" -eq 1 ]; then continue; fi

            filtre_renommerInode "$inode"

        elif [ -L "$inode" ];then
            echo "$INFO lien symbolique. On ne modifie pas.$NORMAL"
            ((inodeCompteur['liens']++));
        fi

    done

    # - on change le nom du repertoire lui meme - #
    #echo "$LINENO:ON RENOMME LE REPERTOIRE:$repertoireLocal";
    filtre_renommerInode $repertoireLocal;


    # - retour au repertoire précédent - #
    showDebug "${tabulation}$LINENO" "cd ..";
    cd ..

    ((profondeurNb--));
    repertoireLocal="$PWD";

    #tabulation="$tabulation  "
    setTabulation;
}


##############
# copieRep #
##############
copieRep(){
    echo ""
    echo "## OPERATION DE COPIE ##"

    # on rajoute le slashe de fin (suppression si existant avant pour eviter un doublon)
    repertoire_source=${repertoire_source%/}
    repertoire_source="$repertoire_source/"
    repertoire_destination=${repertoire_destination%/}
    repertoire_destination="$repertoire_destination/"

    echo "repertoire_source:$repertoire_source"
    echo "repertoire_destination:$repertoire_destination"

    if [ ! -d "$repertoire_source" ];
    then
        echo "${WARN}$repertoire_source n'est pas un repertoire"$NORMAL
        return
    fi

    # creation du repertoire de destination
    if [ ! -d "$repertoire_destination" ];
    then
        showDebug "Repertoire de destination inexistant -> creation"
        evalCmd "mkdir -p \"$repertoire_destination\""
    else
        showDebug "Le repertoire de destination $repertoire_destination existe deja"
    fi

    # Copie des REPERTOIRES #
    # copie des sous repertoires uniquement si la recursivité est demandé
    if [ $isRecursif -eq 1 ];
    then
        echo "copie des repertoire $repertoire_source vers $repertoire_destination"
        evalCmd "find \"$repertoire_source\" -type d 2>/dev/null > /tmp/copieTo-dir.txt"

        #on liste tous les fichiers du repertoire dans un tableau
        readarray fichierList < /tmp/copieTo-dir.txt

        #trie du tableau
        IFS=$'\n' sorted=($(sort <<<"${fichierList[*]}"))
        unset IFS
        unset fichierList

        for repertoire in "${sorted[@]}";
        do
            repertoire_source_relatif="${repertoire/$repertoire_source}"  # suppression de la source root
            repertoire_source_relatif="${repertoire_source_relatif#/}"    # suppression du slashe root
            #echo repertoire_source_relatif="$repertoire_source_relatif"

            repertoire_final="$repertoire_destination$repertoire_source_relatif"
            #echo "repertoire_final=$repertoire_final"

            if [ ! -d $repertoire_final ];
            then
                evalCmd "mkdir -p \"$repertoire_final\""
                ((inodeCompteur['repertoires_copies']++));
            else
                echo $INFO"'$repertoire_final' existe deja."$NORMAL
                ((inodeCompteur['repertoires_copies_existant']++))
            fi
        done
    fi


    # Copie des FICHIERS #
    echo "copie des FICHIERS $repertoire_source vers $repertoire_destination"
    #evalCmd "find $repertoire_source -type l,f 2>/dev/null > /tmp/copieTo-file.txt"
    evalCmd "find \"$repertoire_source\" -type f 2>/dev/null > /tmp/copieTo-file.txt"

    #on liste tous les fichiers du repertoire dans un tableau
    readarray fichierList < /tmp/copieTo-file.txt

    #trie du tableau
    IFS=$'\n' sorted=($(sort --ignore-case --ignore-nonprinting <<<"${fichierList[*]}"))
    unset IFS
    unset fichierList

    local out="";
    local rep="";
    local filename="";
    local index=0;
    for fichier_path in "${sorted[@]}";
    do
        #echo "index: $index"
        #echo "fichier_path= $fichier_path"

        #if [ $index -gt 10 ];then break; fi # for debug

        # - calcul de la source relative - #
        #(suppression du repertoire source dans le chemin du fichier)
        fichier_source_relatif="${fichier_path/$repertoire_source}"

        # suppression du slash du debut (pour evite le root)
        fichier_source_relatif=${fichier_source_relatif#/}  
        #echo "fichier_source_relatif=$fichier_source_relatif"

        # si pas recursif et qu'il y a un slash alors on passe au fichier suivant
        if [ $isRecursif -eq 0 ];
        then
            slashNb=$(echo "$fichier_source_relatif" | grep -o '/' | wc -l)
            #echo "slashNb=$slashNb";
            if [ $slashNb -gt 0 ];
            then
                continue;
            fi
        fi

        fichier_path_destination="$repertoire_destination$fichier_source_relatif"
        #echo "fichier_path_destination=$fichier_path_destination"

        if [ ! -f $fichier_path_destination ];
        then
            evalCmd "cp  \"$fichier_path\" \"$fichier_path_destination\"";
            ((inodeCompteur['fichiers_copies']++));
        else
            echo $INFO"'$fichier_path_destination' existe deja."$NORMAL
            ((inodeCompteur['fichiers_copies_existant']++))
        fi
        ((index++))

    done;
    #echo -e "$out";
}


# ########## #
# parametres #
# ########## #
TEMP=`getopt \
     --options v::dVhnpRc:: \
     --long verbose::,debug,showVars,showLibs,version,help,update,conf,profondeur_max::,createTest,test,rename,copyTo:: \
     -- "$@"`
# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"
#if [ $? -eq 0 ] ; then echo "options requise. Sortie" >&2 ; exit 1 ; fi
#echo "$couleurWARN nb: $# $couleurNORMAL";
#echo "${INFO}argNb:$NORMAL $#"
#echo "${INFO}arg:$NORMAL $?"

while true ; do
    case "$1" in

        # - fonctions generiques - #
        -h|--help) usage; exit 0; shift ;;
        -V|--version) echo "$VERSION"; exit 0; shift ;;
        -v|--verbose)
            case "$2" in
                "") verbosity=1; shift 2 ;;
                *)  #echo "Option c, argument \`$2'" ;
                verbosity=$2; shift 2;;
            esac ;;
         # - Debug - #
        -d|--debug)
            isDebug=1;
            isDebugShowVars=1;
            shift
            ;;
        --showVars)isDebugShowVars=1;   shift ;;

        # - Mise a jours - #
        --update)  isUpdate=1;          shift ;;

        # - Librairies - #
        --showLibs) isShowLibs=1;         shift ;;

        # - Chargement d'un fichier de configuration - #
        --conf)  isConf=1;            shift ;;


        # - parametres liés au projet - #

        --createTest)
            isCreateTest=1;
            createTest
            shift;;
        --test)
            isTest=1;
            lanceTest
            shift;;

        --rename)
            isRename=1;
            shift;;

        -c|--copyTo)
            isCopie=1;
            repertoire_destination=$2;

            if [ "$repertoire_source" == "" ];
            then
                showDebug "Le repertoire source est le repertoire courant"
                repertoire_source='./';
            else
                echo "repertoire source sensé etre defini:"$repertoire_source

            fi
            shift 2;;

        -R)
            isRecursif=1;
            shift ;;

        -p|--profondeur_max )
            # on accepte la valeur demandé
            PROFONDEUR_MAX=$2;

            # on limite les valeurs si elle depasse
            if [ $2 -lt 0 ]
            then
                PROFONDEUR_MAX=0;
                echo "$WARN PROFONDEUR_MAX=$PROFONDEUR_MAX$NORMAL"
            fi
            if [ $2 -gt 9 ];
            then
                PROFONDEUR_MAX=9;
                echo "$WARN PROFONDEUR_MAX=$PROFONDEUR_MAX$NORMAL"
            fi
            shift 2;;

 #       --)
 #           [ -z ${2+x} ] && repertoire_source='' || repertoire_source="$2"
 #           #echo "repertoire_source=$repertoire_source"
 #           shift;
 #           break ;;
 
        # - Les paraletres supplementaires - #
        --)
            if [ -z ${2+x} ];
            then
                PSP_sup="";
                #si aucune source de defini et que l'action copie est pas definit
                # on reinit la source
                #if [ $isCopie -eq 0 ];
                #then
                #    repertoire_source=''
                #fi
            else

                PSP_sup="$2";
                repertoire_source="$2"

                # Methode avec plusieurs push donné (plus tard)
                # - explode - #
                #IFSold=$IFS;
                #IFS=" ";
                ##echo "IFS actuel:" `set | grep ^IFS=`
                ##echo "chaineAvecVirgule=$chaineAvecVirgule";
                #for valeur in $pushRecut;
                #do
                #    pushList[$pushIndex]=$valeur;
                #    ((pushIndex++));
                #done

                #IFS="$IFSold";
            fi

           shift ; break ;;
        
       *) #option invalide
            echo "option $1 non reconnu";
            usage;
            exit $E_ARG_NONE # sans exit: si la derniere option est inconnu -> boucle sans fin
            shift;
            ;;
        esac
done


########
# main #
########
#clear;
#echo "$INFO####";
#echo "${INFO}SCRIPT_REP:$NORMAL $SCRIPT_REP";
echo "${INFO}$SCRIPT_PATH $VERSION$NORMAL";
#echo "${INFO}SCRIPT_FILENAME:$NORMAL $SCRIPT_FILENAME";
#echo "${INFO}SCRIPT_NAME:$NORMAL $SCRIPT_NAME";

# - 0- usage - #
usage;

# - 1- mise a jours du programme - #
selfUpdate;

# - Affichage des variables - #
showVars;


# - sortie en cas d'install/mise a jours - #
if [ $isUpdate -eq 1 ]; then
    exit 0;
fi

showLibs;

# - Traitement du PSP - #
#isLibExist "$PSP_sup"

if [ "$repertoire_source" == "" ];
then
    if [ $isCopie -eq 0 ];
    then
        exit $E_ARG_BAD;
    fi
fi

if [ ! -d "$repertoire_source" ]
then
    echo "$WARN $repertoire_source n'est pas un repertoire!$NORMAL"
    exit $E_ARG_BAD;
else
    showDebug "${tabulation}$LINENO '$repertoire_source' est bien un repertoire.";
fi

if [ $isRename -eq 1 ];
then
    renameRep "$repertoire_source"
fi

if [ $isCopie -eq 1 ];
then
    copieRep    
fi
# resumé #
echo ""
echo "Compteurs:"
echo "  repertoires:";
echo "   - lus        : ${inodeCompteur['repertoires_lus']}";
echo "   - modifies   : ${inodeCompteur['repertoires_modifies']}";
echo "  fichiers";
echo "   - lus        : ${inodeCompteur['fichiers_lus']}";
echo "   - modifiés   : ${inodeCompteur['fichiers_modifies']}";
echo "   - supprimés  : ${inodeCompteur['fichiers_supprimes']}";
echo "  liens         : ${inodeCompteur['liens']}";
echo "copie"
echo "   - repertoires:           ${inodeCompteur['repertoires_copies']}";
echo "   - repertoires existants: ${inodeCompteur['repertoires_copies_existant']}";
echo "   - fichiers:              ${inodeCompteur['fichiers_copies']}";
echo "   - fichiers existants:    ${inodeCompteur['fichiers_copies_existant']}";

exit 0 # https://abs.traduc.org/abs-fr/apd.html#exitcodesref
