#!/bin/bash
# creer un repertoire /tmp/test/renommer
# avec des repertoires et fichiers test

declare -r testDir="/tmp/test/renommer";

evalCmd (){
    cmd=$1;
    echo $CMD $cmd $NORMAL;
    eval $cmd;
}

echo "$LINENO:testDir=$testDir";
# on supprime le rep test existant
if [ -d "$testDir" ];
then
    rm -R "$testDir"
fi

# on (re)cré le rep test
evalCmd "mkdir -p $testDir"

if [ ! -d "$testDir" ];
then
    echo "Erreur lors de la creation de $testDir";
    exit 1
fi

evalCmd "cd $testDir"

touch "a+b.ext"
touch "a b .ext"

touch "a [b].ext"
